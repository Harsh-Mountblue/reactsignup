import React, { Component } from "react";
const validator = require("validator");

export class Input extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            email: "",
            password: "",
            confirmPassword: "",
            success: "",
            nameError: "",
            emailError: "",
            passwordError: "",
            cfPasswordError: "",
            errorStyleName: "",
            errorStyleEmail: "",
            errorStylePassword: "",
            errorStyleCfPassword: "",
        };
    }

    submit = (e) => {
        e.preventDefault();

        if (this.state.name) {
            this.setState({ nameError: "" });
        } else {
            this.setState({
                nameError: "Please Enter name",
                success: "",
                errorStyleName: "2px solid red",
            });
        }

        if (validator.isEmail(this.state.email)) {
            this.setState({ emailError: "" });
        } else {
            this.setState({ errorStyleEmail: "2px solid red" });
            if (this.state.email) {
                this.setState({
                    emailError: "provided email id is wrong",
                    success: "",
                });
            } else {
                this.setState({
                    emailError: "Please provide email id",
                    success: "",
                });
            }
        }

        if (this.state.password.length >= 8) {
            this.setState({ passwordError: "" });
        } else {
            this.setState({ errorStylePassword: "2px solid red" });
            if (this.state.password) {
                this.setState({
                    passwordError: "Password can be of minimum 8 characters",
                    success: "",
                });
            } else {
                this.setState({
                    passwordError: "Please enter password",
                    success: "",
                });
            }
        }

        if (this.state.password === this.state.confirmPassword) {
            this.setState({
                cfPasswordError: "",
            });
        } else {
            this.setState({
                errorStyleCfPassword: "2px solid red",
                cfPasswordError: "Password and confirm password are different",
                success: "",
            });
        }

        if (this.state.name) {
            if (validator.isEmail(this.state.email)) {
                if (this.state.password.length >= 8) {
                    if (this.state.password === this.state.confirmPassword) {
                        this.setState({
                            error: "",
                            success: "Successfully Signed Up!!!",
                        });
                        this.props.save(this.state);
                        this.setState({
                            name: "",
                            email: "",
                            password: "",
                            confirmPassword: "",
                            nameError: "",
                            emailError: "",
                            passwordError: "",
                            cfPasswordError: "",
                            errorStyleName: "",
                            errorStyleEmail: "",
                            errorStylePassword: "",
                            errorStyleCfPassword: "",
                        });
                    }
                }
            }
        }
    };

    change = (event) => {
        switch (event.target.id) {
            case "name":
                this.setState({
                    nameError: "",
                });
                this.setState({ name: event.target.value });
                if (event.target.value) {
                    this.setState({ errorStyleName: "2px solid green" });
                } else {
                    this.setState({ errorStyleName: "2px solid red" });
                }
                break;
            case "email":
                this.setState({ emailError: "" });
                this.setState({ email: event.target.value });
                if (validator.isEmail(event.target.value)) {
                    this.setState({ errorStyleEmail: "2px solid green" });
                } else {
                    this.setState({ errorStyleEmail: "2px solid red" });
                }
                break;
            case "password":
                this.setState({ passwordError: "" });
                this.setState({ password: event.target.value });
                if (event.target.value.length >= 8) {
                    this.setState({ errorStylePassword: "2px solid green" });
                } else {
                    this.setState({ errorStylePassword: "2px solid red" });
                }
                break;
            case "confirmPassword":
                this.setState({ confirmPassword: event.target.value });
                if (
                    event.target.value.length >= 8 &&
                    event.target.value === this.state.password
                ) {
                    this.setState({
                        errorStyleCfPassword: "2px solid green",
                        cfPasswordError: "",
                    });
                } else {
                    this.setState({ errorStyleCfPassword: "2px solid red" });
                }
                break;
            default:
                break;
        }
        // console.log(event.target.value);
    };

    render() {
        return (
            <div>
                <h1 style={{ textAlign: "center" }}> Sign Up Form </h1>
                <form onSubmit={this.submit}>
                    <div>
                        {this.state.success === "" ? (
                            ""
                        ) : (
                            <p
                                className="alert alert-success"
                                // style={{ color: "green", textAlign: "center" }}
                            >
                                {" "}
                                {this.state.success}{" "}
                            </p>
                        )}
                    </div>

                    <label>
                        Name:{" "}
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "column",
                                // justifyContent: "space-evenly",
                            }}
                        >
                            {/* <i
                                class="fa fa-user"
                                style={{ fontSize: "25px", padding: "1%" }}
                            ></i> */}
                            <input
                                className="form-floating"
                                for="floatingInputInvalid"
                                onChange={this.change}
                                id="name"
                                type="text"
                                placeholder="Enter Name"
                                style={{
                                    fontSize: "15px",
                                    border: `${this.state.errorStyleName}`,
                                }}
                                value={this.state.name}
                            ></input>
                            <div style={{ color: "red", fontSize: "12px" }}>
                                {this.state.nameError}
                            </div>
                        </div>
                    </label>
                    <label>
                        Email:{" "}
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "column",
                                // justifyContent: "space-evenly",
                            }}
                        >
                            {/* <i
                                class="fa fa-envelope-square"
                                style={{ fontSize: "25px", padding: "1%" }}
                            ></i> */}
                            <input
                                onChange={this.change}
                                id="email"
                                placeholder="Enter Email id"
                                style={{
                                    fontSize: "15px",
                                    border: `${this.state.errorStyleEmail}`,
                                }}
                                value={this.state.email}
                            ></input>
                            <div style={{ color: "red", fontSize: "12px" }}>
                                {this.state.emailError}
                            </div>
                        </div>
                    </label>
                    <label>
                        Password:{" "}
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "column",
                                // justifyContent: "space-evenly",
                            }}
                        >
                            {/* <i
                                class="fa fa-lock"
                                style={{ fontSize: "25px", padding: "1%" }}
                            ></i> */}
                            <input
                                onChange={this.change}
                                id="password"
                                type="password"
                                placeholder="Enter Password"
                                style={{
                                    fontSize: "15px",
                                    border: `${this.state.errorStylePassword}`,
                                }}
                                value={this.state.password}
                            ></input>
                            <div style={{ color: "red", fontSize: "12px" }}>
                                {this.state.passwordError}
                            </div>
                        </div>
                    </label>
                    <label>
                        Confirm Password:
                        <div
                            style={{
                                display: "flex",
                                flexDirection: "column",
                                // justifyContent: "space-evenly",
                            }}
                        >
                            {/* <i
                                class="fa fa-lock"
                                style={{ fontSize: "25px", padding: "1%" }}
                            ></i> */}
                            <input
                                onChange={this.change}
                                id="confirmPassword"
                                type="password"
                                placeholder="Repeat Password"
                                style={{
                                    fontSize: "15px",
                                    border: `${this.state.errorStyleCfPassword}`,
                                }}
                                value={this.state.confirmPassword}
                            ></input>
                            <div style={{ color: "red", fontSize: "12px" }}>
                                {this.state.cfPasswordError}
                            </div>
                        </div>
                    </label>
                    <input
                        type="submit"
                        value="Submit"
                        className="btn btn-primary"
                        style={{
                            backgroundColor: "green",
                            borderRadius: "15px",
                            width: "200px",
                            height: "40px",
                            margin: "auto",
                            marginTop: "15px",
                        }}
                    />
                </form>
            </div>
        );
    }
}

export default Input;
