import "./App.css";
import React, { Component } from "react";
import Input from "./components/Input";

export class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: "",
            email: "",
            password: "",
            confirmPassword: "",
        };
    }

    saveData = (value) => {
        this.setState({
            name: value.name,
            email: value.email,
            password: value.password,
            confirmPassword: value.confirmPassword,
        });
    };

    render() {
        return (
            <div className="App">
                <Input save={this.saveData.bind(this)} />
            </div>
        );
    }
}

export default App;
